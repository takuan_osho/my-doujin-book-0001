var gulp     = require('gulp');
var textlint = require('gulp-textlint');
var plumber  = require('gulp-plumber');

gulp.task('textlint', function() {
  return gulp.src('source/**/*.rst')
    .pipe(plumber())
    .pipe(textlint({
      formatterName: "pretty-error"
    }));
});

gulp.task('watch', function() {
  gulp.watch('source/**/*.rst', ['textlint'])
});
